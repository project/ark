<?php

/**
 * Provides form to define settings.
 *
 * Settings which must be set include:
 *  - Naming Assigning Authority Number
 *  - Content types
 *  - Fields
 */
function ark_settings($form, &$form_state) {

  $form['ark_naan'] = array(
    '#type' => 'textfield',
    '#title' => t('Name Assigning Authority Number'),
    '#description' => t('Before your ARK identifiers can be shared publicly, a NAAN must be provided to you by the <a href="http://www.cdlib.org/">California Digital Library</a>. See <a href="https://wiki.ucop.edu/display/Curation/ARK#ARK-NAAN:theNameAssigningAuthorityNumber">the ARK wiki for more information</a>. Use "pending" here to test your site before you have a number.'),
    '#default_value' => variable_get('ark_naan', 'pending'),
    '#size' => 10,
    '#required' => TRUE,
  );

  $form['ark_name_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix for ARK names'),
    '#description' => t('Every name assigning authority must have a namespace management strategy. A time-honored technique is to hierarchically partition a namespace into subnamespaces using prefixes that guarantee non-collision of names in different partition. This practice is strongly encouraged for all NAAs, especially when subnamespace management will be delegated to other departments, units, or projects within an organization.'),
    '#default_value' => variable_get('ark_name_prefix', ark_default_prefix()),
    '#size' => 10,
  );

  $node_types = ark_all_available_node_types();
  $form['ark_node_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Content types'),
    '#description' => t('Select the content types in which every piece of content should receive Archival Resource Key identifiers.'),
    '#options' => $node_types,
    '#default_value' => variable_get('ark_node_types', ''),
  );

  $form['fields_explanation'] = array(
    '#markup' => '<p>' . t('Select the fields to use for Who (a textfield) and When (a datefield) for each resource/content type. (What is always matched to the content title.)') . '</p>',
  );

  foreach ($node_types as $type_machine_name => $type_human) {
    $who = 'ark_' . $type_machine_name . '_who';
    $form[$who] = array(
      '#type' => 'select',
      '#title' => $type_human . ' ' . t('Who'),
      '#options' => ark_fields_by_type_by_bundle('node', $type_machine_name, 'text'),
      '#default_value' => variable_get($who),
    );

    $when = 'ark_' . $type_machine_name . '_when';
    $form[$when] = array(
      '#type' => 'select',
      '#title' => $type_human . ' ' . t('When'),
      '#options' => ark_fields_by_type_by_bundle('node', $type_machine_name, 'date'),
      '#default_value' => variable_get($when),
    );

  }

  $form['#validate'][] = 'ark_settings_validate';

  return system_settings_form($form);
}

/**
 * Form validation logic for the contact form.
 */
function ark_settings_validate($form, &$form_state) {
  if (!ark_naan_format_valid($form_state['values']['ark_naan'])) {
    form_set_error('ark_naan', t('NAAN must be a number, or, if you do not yet have one, test the site using "pending".'));
  }
  if (!$form_state['values']['ark_node_types']) {
    form_set_error('ark_node_types', t('You must select at least one content type to use as a resource.'));
  }
  else {
    foreach ($form_state['values']['ark_node_types'] as $type_machine_name => $type_human) {
      if (!$form_state['values']['ark_' . $type_machine_name . '_who']) {
        form_set_error('ark_' . $type_machine_name . '_who', t('There must be a text field on the selected %type_human content type used for "Who".', array('%type_human' => $type_human)));
      }
      if (!$form_state['values']['ark_' . $type_machine_name . '_when']) {
        form_set_error('ark_' . $type_machine_name . '_when', t('There must be a date field on the selected %type_human content type used for "When".', array('%type_human' => $type_human)));
      }
    }
  }
}

/**
 * Returns all available node types defined on the site.
 */
function ark_all_available_node_types() {
  return _node_types_build()->names;
}

/**
 * Helper function to return all fields of one type on one bundle.
 */
function ark_fields_by_type_by_bundle($entity_type, $bundle, $field_type) {
  $result_fields = array();
  $fields = field_info_field_map();
  foreach ($fields as $field => $info) {
    if ($info['type'] == $field_type &&
        in_array($entity_type, array_keys($info['bundles'])) &&
        in_array($bundle, $info['bundles'][$entity_type])) {
      $result_fields[$field] = $field;
    }
  }
  if (!$result_fields) {
    $result_fields[''] = t('The @entity_type @bundle needs a @field_type field to be used.',
        array('@entity_type' => $entity_type, '@bundle' => $bundle, '@field_type' => $field_type));
  }
  return $result_fields;
}

/**
 * Provide a default ARK prefix (random, as recommended by ARK).
 */
function ark_default_prefix() {
  return substr(str_shuffle('bcdfghjklmnpqrstvwxyz123456789'), 0, 4);
}
